﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace soapTests.DocumentService {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://ws.oscarehr.org/", ConfigurationName="DocumentService.DocumentWs")]
    public interface DocumentWs {
        
        // CODEGEN: Parameter 'return' requires additional schema information that cannot be captured using the parameter mode. The specific attribute is 'System.Xml.Serialization.XmlElementAttribute'.
        [System.ServiceModel.OperationContractAttribute(Action="", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        [return: System.ServiceModel.MessageParameterAttribute(Name="return")]
        soapTests.DocumentService.addDocumentResponse addDocument(soapTests.DocumentService.addDocument request);
        
        [System.ServiceModel.OperationContractAttribute(Action="", ReplyAction="*")]
        System.Threading.Tasks.Task<soapTests.DocumentService.addDocumentResponse> addDocumentAsync(soapTests.DocumentService.addDocument request);
        
        // CODEGEN: Parameter 'return' requires additional schema information that cannot be captured using the parameter mode. The specific attribute is 'System.Xml.Serialization.XmlElementAttribute'.
        [System.ServiceModel.OperationContractAttribute(Action="", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        [return: System.ServiceModel.MessageParameterAttribute(Name="return")]
        soapTests.DocumentService.getDocumentResponse getDocument(soapTests.DocumentService.getDocument request);
        
        [System.ServiceModel.OperationContractAttribute(Action="", ReplyAction="*")]
        System.Threading.Tasks.Task<soapTests.DocumentService.getDocumentResponse> getDocumentAsync(soapTests.DocumentService.getDocument request);
        
        // CODEGEN: Parameter 'return' requires additional schema information that cannot be captured using the parameter mode. The specific attribute is 'System.Xml.Serialization.XmlElementAttribute'.
        [System.ServiceModel.OperationContractAttribute(Action="", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        [return: System.ServiceModel.MessageParameterAttribute(Name="return")]
        soapTests.DocumentService.getDocumentsUpdateAfterDateResponse getDocumentsUpdateAfterDate(soapTests.DocumentService.getDocumentsUpdateAfterDate request);
        
        [System.ServiceModel.OperationContractAttribute(Action="", ReplyAction="*")]
        System.Threading.Tasks.Task<soapTests.DocumentService.getDocumentsUpdateAfterDateResponse> getDocumentsUpdateAfterDateAsync(soapTests.DocumentService.getDocumentsUpdateAfterDate request);
        
        // CODEGEN: Parameter 'return' requires additional schema information that cannot be captured using the parameter mode. The specific attribute is 'System.Xml.Serialization.XmlElementAttribute'.
        [System.ServiceModel.OperationContractAttribute(Action="", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        [return: System.ServiceModel.MessageParameterAttribute(Name="return")]
        soapTests.DocumentService.getDocumentsByProgramProviderDemographicDateResponse getDocumentsByProgramProviderDemographicDate(soapTests.DocumentService.getDocumentsByProgramProviderDemographicDate request);
        
        [System.ServiceModel.OperationContractAttribute(Action="", ReplyAction="*")]
        System.Threading.Tasks.Task<soapTests.DocumentService.getDocumentsByProgramProviderDemographicDateResponse> getDocumentsByProgramProviderDemographicDateAsync(soapTests.DocumentService.getDocumentsByProgramProviderDemographicDate request);
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.3056.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://ws.oscarehr.org/")]
    public partial class documentTransfer : object, System.ComponentModel.INotifyPropertyChanged {
        
        private int appointmentNoField;
        
        private bool appointmentNoFieldSpecified;
        
        private string contenttypeField;
        
        private string ctlModuleField;
        
        private int ctlModuleIdField;
        
        private bool ctlModuleIdFieldSpecified;
        
        private string ctlStatusField;
        
        private string docClassField;
        
        private string docSubClassField;
        
        private string doccreatorField;
        
        private string docdescField;
        
        private string docfilenameField;
        
        private string doctypeField;
        
        private int documentNoField;
        
        private bool documentNoFieldSpecified;
        
        private string docxmlField;
        
        private byte[] fileContentsField;
        
        private int numberofpagesField;
        
        private bool numberofpagesFieldSpecified;
        
        private System.DateTime observationdateField;
        
        private bool observationdateFieldSpecified;
        
        private int programIdField;
        
        private bool programIdFieldSpecified;
        
        private int public1Field;
        
        private string responsibleField;
        
        private System.DateTime reviewdatetimeField;
        
        private bool reviewdatetimeFieldSpecified;
        
        private string reviewerField;
        
        private string sourceField;
        
        private string sourceFacilityField;
        
        private ushort statusField;
        
        private System.DateTime updatedatetimeField;
        
        private bool updatedatetimeFieldSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=0)]
        public int appointmentNo {
            get {
                return this.appointmentNoField;
            }
            set {
                this.appointmentNoField = value;
                this.RaisePropertyChanged("appointmentNo");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool appointmentNoSpecified {
            get {
                return this.appointmentNoFieldSpecified;
            }
            set {
                this.appointmentNoFieldSpecified = value;
                this.RaisePropertyChanged("appointmentNoSpecified");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=1)]
        public string contenttype {
            get {
                return this.contenttypeField;
            }
            set {
                this.contenttypeField = value;
                this.RaisePropertyChanged("contenttype");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=2)]
        public string ctlModule {
            get {
                return this.ctlModuleField;
            }
            set {
                this.ctlModuleField = value;
                this.RaisePropertyChanged("ctlModule");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=3)]
        public int ctlModuleId {
            get {
                return this.ctlModuleIdField;
            }
            set {
                this.ctlModuleIdField = value;
                this.RaisePropertyChanged("ctlModuleId");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ctlModuleIdSpecified {
            get {
                return this.ctlModuleIdFieldSpecified;
            }
            set {
                this.ctlModuleIdFieldSpecified = value;
                this.RaisePropertyChanged("ctlModuleIdSpecified");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=4)]
        public string ctlStatus {
            get {
                return this.ctlStatusField;
            }
            set {
                this.ctlStatusField = value;
                this.RaisePropertyChanged("ctlStatus");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=5)]
        public string docClass {
            get {
                return this.docClassField;
            }
            set {
                this.docClassField = value;
                this.RaisePropertyChanged("docClass");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=6)]
        public string docSubClass {
            get {
                return this.docSubClassField;
            }
            set {
                this.docSubClassField = value;
                this.RaisePropertyChanged("docSubClass");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=7)]
        public string doccreator {
            get {
                return this.doccreatorField;
            }
            set {
                this.doccreatorField = value;
                this.RaisePropertyChanged("doccreator");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=8)]
        public string docdesc {
            get {
                return this.docdescField;
            }
            set {
                this.docdescField = value;
                this.RaisePropertyChanged("docdesc");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=9)]
        public string docfilename {
            get {
                return this.docfilenameField;
            }
            set {
                this.docfilenameField = value;
                this.RaisePropertyChanged("docfilename");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=10)]
        public string doctype {
            get {
                return this.doctypeField;
            }
            set {
                this.doctypeField = value;
                this.RaisePropertyChanged("doctype");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=11)]
        public int documentNo {
            get {
                return this.documentNoField;
            }
            set {
                this.documentNoField = value;
                this.RaisePropertyChanged("documentNo");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool documentNoSpecified {
            get {
                return this.documentNoFieldSpecified;
            }
            set {
                this.documentNoFieldSpecified = value;
                this.RaisePropertyChanged("documentNoSpecified");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=12)]
        public string docxml {
            get {
                return this.docxmlField;
            }
            set {
                this.docxmlField = value;
                this.RaisePropertyChanged("docxml");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, DataType="base64Binary", Order=13)]
        public byte[] fileContents {
            get {
                return this.fileContentsField;
            }
            set {
                this.fileContentsField = value;
                this.RaisePropertyChanged("fileContents");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=14)]
        public int numberofpages {
            get {
                return this.numberofpagesField;
            }
            set {
                this.numberofpagesField = value;
                this.RaisePropertyChanged("numberofpages");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool numberofpagesSpecified {
            get {
                return this.numberofpagesFieldSpecified;
            }
            set {
                this.numberofpagesFieldSpecified = value;
                this.RaisePropertyChanged("numberofpagesSpecified");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=15)]
        public System.DateTime observationdate {
            get {
                return this.observationdateField;
            }
            set {
                this.observationdateField = value;
                this.RaisePropertyChanged("observationdate");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool observationdateSpecified {
            get {
                return this.observationdateFieldSpecified;
            }
            set {
                this.observationdateFieldSpecified = value;
                this.RaisePropertyChanged("observationdateSpecified");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=16)]
        public int programId {
            get {
                return this.programIdField;
            }
            set {
                this.programIdField = value;
                this.RaisePropertyChanged("programId");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool programIdSpecified {
            get {
                return this.programIdFieldSpecified;
            }
            set {
                this.programIdFieldSpecified = value;
                this.RaisePropertyChanged("programIdSpecified");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=17)]
        public int public1 {
            get {
                return this.public1Field;
            }
            set {
                this.public1Field = value;
                this.RaisePropertyChanged("public1");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=18)]
        public string responsible {
            get {
                return this.responsibleField;
            }
            set {
                this.responsibleField = value;
                this.RaisePropertyChanged("responsible");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=19)]
        public System.DateTime reviewdatetime {
            get {
                return this.reviewdatetimeField;
            }
            set {
                this.reviewdatetimeField = value;
                this.RaisePropertyChanged("reviewdatetime");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool reviewdatetimeSpecified {
            get {
                return this.reviewdatetimeFieldSpecified;
            }
            set {
                this.reviewdatetimeFieldSpecified = value;
                this.RaisePropertyChanged("reviewdatetimeSpecified");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=20)]
        public string reviewer {
            get {
                return this.reviewerField;
            }
            set {
                this.reviewerField = value;
                this.RaisePropertyChanged("reviewer");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=21)]
        public string source {
            get {
                return this.sourceField;
            }
            set {
                this.sourceField = value;
                this.RaisePropertyChanged("source");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=22)]
        public string sourceFacility {
            get {
                return this.sourceFacilityField;
            }
            set {
                this.sourceFacilityField = value;
                this.RaisePropertyChanged("sourceFacility");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=23)]
        public ushort status {
            get {
                return this.statusField;
            }
            set {
                this.statusField = value;
                this.RaisePropertyChanged("status");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=24)]
        public System.DateTime updatedatetime {
            get {
                return this.updatedatetimeField;
            }
            set {
                this.updatedatetimeField = value;
                this.RaisePropertyChanged("updatedatetime");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool updatedatetimeSpecified {
            get {
                return this.updatedatetimeFieldSpecified;
            }
            set {
                this.updatedatetimeFieldSpecified = value;
                this.RaisePropertyChanged("updatedatetimeSpecified");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="addDocument", WrapperNamespace="http://ws.oscarehr.org/", IsWrapped=true)]
    public partial class addDocument {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://ws.oscarehr.org/", Order=0)]
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public soapTests.DocumentService.documentTransfer arg0;
        
        public addDocument() {
        }
        
        public addDocument(soapTests.DocumentService.documentTransfer arg0) {
            this.arg0 = arg0;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="addDocumentResponse", WrapperNamespace="http://ws.oscarehr.org/", IsWrapped=true)]
    public partial class addDocumentResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://ws.oscarehr.org/", Order=0)]
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public int @return;
        
        public addDocumentResponse() {
        }
        
        public addDocumentResponse(int @return) {
            this.@return = @return;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="getDocument", WrapperNamespace="http://ws.oscarehr.org/", IsWrapped=true)]
    public partial class getDocument {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://ws.oscarehr.org/", Order=0)]
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public int arg0;
        
        public getDocument() {
        }
        
        public getDocument(int arg0) {
            this.arg0 = arg0;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="getDocumentResponse", WrapperNamespace="http://ws.oscarehr.org/", IsWrapped=true)]
    public partial class getDocumentResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://ws.oscarehr.org/", Order=0)]
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public soapTests.DocumentService.documentTransfer @return;
        
        public getDocumentResponse() {
        }
        
        public getDocumentResponse(soapTests.DocumentService.documentTransfer @return) {
            this.@return = @return;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="getDocumentsUpdateAfterDate", WrapperNamespace="http://ws.oscarehr.org/", IsWrapped=true)]
    public partial class getDocumentsUpdateAfterDate {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://ws.oscarehr.org/", Order=0)]
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public System.DateTime arg0;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://ws.oscarehr.org/", Order=1)]
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public int arg1;
        
        public getDocumentsUpdateAfterDate() {
        }
        
        public getDocumentsUpdateAfterDate(System.DateTime arg0, int arg1) {
            this.arg0 = arg0;
            this.arg1 = arg1;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="getDocumentsUpdateAfterDateResponse", WrapperNamespace="http://ws.oscarehr.org/", IsWrapped=true)]
    public partial class getDocumentsUpdateAfterDateResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://ws.oscarehr.org/", Order=0)]
        [System.Xml.Serialization.XmlElementAttribute("return", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public soapTests.DocumentService.documentTransfer[] @return;
        
        public getDocumentsUpdateAfterDateResponse() {
        }
        
        public getDocumentsUpdateAfterDateResponse(soapTests.DocumentService.documentTransfer[] @return) {
            this.@return = @return;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="getDocumentsByProgramProviderDemographicDate", WrapperNamespace="http://ws.oscarehr.org/", IsWrapped=true)]
    public partial class getDocumentsByProgramProviderDemographicDate {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://ws.oscarehr.org/", Order=0)]
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public int arg0;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://ws.oscarehr.org/", Order=1)]
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string arg1;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://ws.oscarehr.org/", Order=2)]
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public int arg2;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://ws.oscarehr.org/", Order=3)]
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public System.DateTime arg3;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://ws.oscarehr.org/", Order=4)]
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public int arg4;
        
        public getDocumentsByProgramProviderDemographicDate() {
        }
        
        public getDocumentsByProgramProviderDemographicDate(int arg0, string arg1, int arg2, System.DateTime arg3, int arg4) {
            this.arg0 = arg0;
            this.arg1 = arg1;
            this.arg2 = arg2;
            this.arg3 = arg3;
            this.arg4 = arg4;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="getDocumentsByProgramProviderDemographicDateResponse", WrapperNamespace="http://ws.oscarehr.org/", IsWrapped=true)]
    public partial class getDocumentsByProgramProviderDemographicDateResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://ws.oscarehr.org/", Order=0)]
        [System.Xml.Serialization.XmlElementAttribute("return", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public soapTests.DocumentService.documentTransfer[] @return;
        
        public getDocumentsByProgramProviderDemographicDateResponse() {
        }
        
        public getDocumentsByProgramProviderDemographicDateResponse(soapTests.DocumentService.documentTransfer[] @return) {
            this.@return = @return;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface DocumentWsChannel : soapTests.DocumentService.DocumentWs, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class DocumentWsClient : System.ServiceModel.ClientBase<soapTests.DocumentService.DocumentWs>, soapTests.DocumentService.DocumentWs {
        
        public DocumentWsClient() {
        }
        
        public DocumentWsClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public DocumentWsClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public DocumentWsClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public DocumentWsClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        soapTests.DocumentService.addDocumentResponse soapTests.DocumentService.DocumentWs.addDocument(soapTests.DocumentService.addDocument request) {
            return base.Channel.addDocument(request);
        }
        
        public int addDocument(soapTests.DocumentService.documentTransfer arg0) {
            soapTests.DocumentService.addDocument inValue = new soapTests.DocumentService.addDocument();
            inValue.arg0 = arg0;
            soapTests.DocumentService.addDocumentResponse retVal = ((soapTests.DocumentService.DocumentWs)(this)).addDocument(inValue);
            return retVal.@return;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<soapTests.DocumentService.addDocumentResponse> soapTests.DocumentService.DocumentWs.addDocumentAsync(soapTests.DocumentService.addDocument request) {
            return base.Channel.addDocumentAsync(request);
        }
        
        public System.Threading.Tasks.Task<soapTests.DocumentService.addDocumentResponse> addDocumentAsync(soapTests.DocumentService.documentTransfer arg0) {
            soapTests.DocumentService.addDocument inValue = new soapTests.DocumentService.addDocument();
            inValue.arg0 = arg0;
            return ((soapTests.DocumentService.DocumentWs)(this)).addDocumentAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        soapTests.DocumentService.getDocumentResponse soapTests.DocumentService.DocumentWs.getDocument(soapTests.DocumentService.getDocument request) {
            return base.Channel.getDocument(request);
        }
        
        public soapTests.DocumentService.documentTransfer getDocument(int arg0) {
            soapTests.DocumentService.getDocument inValue = new soapTests.DocumentService.getDocument();
            inValue.arg0 = arg0;
            soapTests.DocumentService.getDocumentResponse retVal = ((soapTests.DocumentService.DocumentWs)(this)).getDocument(inValue);
            return retVal.@return;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<soapTests.DocumentService.getDocumentResponse> soapTests.DocumentService.DocumentWs.getDocumentAsync(soapTests.DocumentService.getDocument request) {
            return base.Channel.getDocumentAsync(request);
        }
        
        public System.Threading.Tasks.Task<soapTests.DocumentService.getDocumentResponse> getDocumentAsync(int arg0) {
            soapTests.DocumentService.getDocument inValue = new soapTests.DocumentService.getDocument();
            inValue.arg0 = arg0;
            return ((soapTests.DocumentService.DocumentWs)(this)).getDocumentAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        soapTests.DocumentService.getDocumentsUpdateAfterDateResponse soapTests.DocumentService.DocumentWs.getDocumentsUpdateAfterDate(soapTests.DocumentService.getDocumentsUpdateAfterDate request) {
            return base.Channel.getDocumentsUpdateAfterDate(request);
        }
        
        public soapTests.DocumentService.documentTransfer[] getDocumentsUpdateAfterDate(System.DateTime arg0, int arg1) {
            soapTests.DocumentService.getDocumentsUpdateAfterDate inValue = new soapTests.DocumentService.getDocumentsUpdateAfterDate();
            inValue.arg0 = arg0;
            inValue.arg1 = arg1;
            soapTests.DocumentService.getDocumentsUpdateAfterDateResponse retVal = ((soapTests.DocumentService.DocumentWs)(this)).getDocumentsUpdateAfterDate(inValue);
            return retVal.@return;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<soapTests.DocumentService.getDocumentsUpdateAfterDateResponse> soapTests.DocumentService.DocumentWs.getDocumentsUpdateAfterDateAsync(soapTests.DocumentService.getDocumentsUpdateAfterDate request) {
            return base.Channel.getDocumentsUpdateAfterDateAsync(request);
        }
        
        public System.Threading.Tasks.Task<soapTests.DocumentService.getDocumentsUpdateAfterDateResponse> getDocumentsUpdateAfterDateAsync(System.DateTime arg0, int arg1) {
            soapTests.DocumentService.getDocumentsUpdateAfterDate inValue = new soapTests.DocumentService.getDocumentsUpdateAfterDate();
            inValue.arg0 = arg0;
            inValue.arg1 = arg1;
            return ((soapTests.DocumentService.DocumentWs)(this)).getDocumentsUpdateAfterDateAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        soapTests.DocumentService.getDocumentsByProgramProviderDemographicDateResponse soapTests.DocumentService.DocumentWs.getDocumentsByProgramProviderDemographicDate(soapTests.DocumentService.getDocumentsByProgramProviderDemographicDate request) {
            return base.Channel.getDocumentsByProgramProviderDemographicDate(request);
        }
        
        public soapTests.DocumentService.documentTransfer[] getDocumentsByProgramProviderDemographicDate(int arg0, string arg1, int arg2, System.DateTime arg3, int arg4) {
            soapTests.DocumentService.getDocumentsByProgramProviderDemographicDate inValue = new soapTests.DocumentService.getDocumentsByProgramProviderDemographicDate();
            inValue.arg0 = arg0;
            inValue.arg1 = arg1;
            inValue.arg2 = arg2;
            inValue.arg3 = arg3;
            inValue.arg4 = arg4;
            soapTests.DocumentService.getDocumentsByProgramProviderDemographicDateResponse retVal = ((soapTests.DocumentService.DocumentWs)(this)).getDocumentsByProgramProviderDemographicDate(inValue);
            return retVal.@return;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<soapTests.DocumentService.getDocumentsByProgramProviderDemographicDateResponse> soapTests.DocumentService.DocumentWs.getDocumentsByProgramProviderDemographicDateAsync(soapTests.DocumentService.getDocumentsByProgramProviderDemographicDate request) {
            return base.Channel.getDocumentsByProgramProviderDemographicDateAsync(request);
        }
        
        public System.Threading.Tasks.Task<soapTests.DocumentService.getDocumentsByProgramProviderDemographicDateResponse> getDocumentsByProgramProviderDemographicDateAsync(int arg0, string arg1, int arg2, System.DateTime arg3, int arg4) {
            soapTests.DocumentService.getDocumentsByProgramProviderDemographicDate inValue = new soapTests.DocumentService.getDocumentsByProgramProviderDemographicDate();
            inValue.arg0 = arg0;
            inValue.arg1 = arg1;
            inValue.arg2 = arg2;
            inValue.arg3 = arg3;
            inValue.arg4 = arg4;
            return ((soapTests.DocumentService.DocumentWs)(this)).getDocumentsByProgramProviderDemographicDateAsync(inValue);
        }
    }
}
