﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace soapTests
{
    class Program
    {
        public class MyBehavior : IEndpointBehavior
        {

            private LoginService.loginResultTransfer2 security;

            public MyBehavior(LoginService.loginResultTransfer2 security = null)
            {
                this.security = security;
            }

            public void AddBindingParameters(
                ServiceEndpoint endpoint,
                BindingParameterCollection bindingParameters)
            {
            }

            public void ApplyClientBehavior(
                ServiceEndpoint endpoint,
                ClientRuntime clientRuntime)
            {
                clientRuntime.MessageInspectors.Add(new MyMessageInspector(security));
            }

            public void ApplyDispatchBehavior(
                ServiceEndpoint endpoint,
                EndpointDispatcher endpointDispatcher)
            {
            }

            public void Validate(
                ServiceEndpoint endpoint)
            {
            }


        }

        public class SecurityInfo
        {

        }

        public class MyMessageInspector : IClientMessageInspector
        {
            private LoginService.loginResultTransfer2 security;

            public MyMessageInspector(LoginService.loginResultTransfer2 security)
            {
                this.security = security;
            }
            public void AfterReceiveReply(
                ref Message reply,
                object correlationState)
            {
                Console.WriteLine(
                "Received the following reply: '{0}'", reply.ToString());
            }


            private void ChangeMessage (ref Message request)
            {

            }


            public object BeforeSendRequest(
                ref Message request,
                IClientChannel channel)
            {
                if (security != null)
                {
                    Console.WriteLine("Adding Security Header");
                    request.Headers.Add(new WSSEHeader(security.securityId.ToString(), security.securityTokenKey));



                  
                    Message newMessage = null;
                    XElement bodyEl = XElement.Load(request.GetReaderAtBodyContents().ReadSubtree());

                    Console.WriteLine("The body: \n");
                    Console.WriteLine(bodyEl.ToString());

                    var node = (from nodes in bodyEl.DescendantsAndSelf()
                                where nodes.Name.NamespaceName == "http://ws.oscarehr.org/"
                                select nodes).FirstOrDefault();


                    if (node is null)
                    {
                       return null;
                    }


                    Console.WriteLine("The extracted node:\n");
                    Console.WriteLine(node.ToString());

                    XNamespace ns = "http://ws.oscarehr.org/";
                    XElement newElement = new XElement(ns + node.Name.LocalName, new XAttribute(XNamespace.Xmlns + "ws", ns));
                    newElement.Add(node.Nodes());


                    Console.WriteLine("The new element: \n");
                    Console.WriteLine(newElement.ToString());

                    MemoryStream ms = new MemoryStream();
                    XmlWriter xw = XmlWriter.Create(ms);
                   
                    newElement.Save(xw);
                  
                    xw.Flush();
                    xw.Close();

                    ms.Position = 0;
                    XmlReader xr = XmlReader.Create(ms);

                    newMessage = Message.CreateMessage(request.Version, null, xr);
                    newMessage.Headers.CopyHeadersFrom(request);
                    newMessage.Properties.CopyProperties(request.Properties);
                    

                    request = newMessage;
                    Console.WriteLine(
                    "Sending the following request: '{0}'", request.ToString());
                }

                return null;
            }

            
        }


        public static LoginService.loginResultTransfer2 security;

        static void Main(string[] args)
        {
            

           /* 
            InfoService.SystemInfoWsClient client = new InfoService.SystemInfoWsClient();
            client.Open();
            var response = client.helloWorld();
            Console.WriteLine(response);
            */

            
           

            LoginService.LoginWsClient client = new LoginService.LoginWsClient();
            client.Endpoint.EndpointBehaviors.Add(new MyBehavior());
            client.Open();
            var result = client.login2("corcare_service", "Xmgg34xm!");
            security = result;
            Console.WriteLine("Security Id: " + result.securityId);
            Console.WriteLine("Security Token: " + result.securityTokenKey);
            Console.ReadLine();

            //AddDocument();

            //AddAppointment();
            //GetAppointments();
            //GetBillingCodeDetails();
            //GetMultiBillingCodeDetails();
            CreateInvoice();
            Console.ReadLine();



        }

        private static void GetProviders()
        {

            oscarProviderService.ProviderWsClient providerClient = new oscarProviderService.ProviderWsClient();
            providerClient.Endpoint.EndpointBehaviors.Add(new MyBehavior(security));
            providerClient.Open();
            try
            {
                var providers = providerClient.getProviders2(true);
                foreach (var provider in providers)
                {
                    Console.WriteLine(provider.firstName);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Caught Exception: " + e.Message);
            }
           
        }

        private static void UploadFile()
        {
            ScheduleService.ScheduleWsClient ss = new ScheduleService.ScheduleWsClient();
            ss.Endpoint.EndpointBehaviors.Add(new MyBehavior(security));
            ss.Open();
            ss.uploadFile(new ScheduleService.fileUploader() {
                dfile = File.ReadAllBytes(@"C:\Users\siyam\Downloads\incoming\hello.txt"),
                fileType = "txt",
                name = "test"
            });
        }

        private static void AddDocument()
        {
            DocumentService.DocumentWsClient ds = new DocumentService.DocumentWsClient();
            ds.Endpoint.EndpointBehaviors.Add(new MyBehavior(security));
            ds.Open();
            ds.addDocument(new DocumentService.documentTransfer()
            {
                ctlModule = "demographic",
                ctlModuleId = 1,
                ctlModuleIdSpecified = true,
                appointmentNoSpecified = false,
                documentNoSpecified = false,
                docfilename = "add_document_test",
                fileContents = File.ReadAllBytes(@"C:\Users\siyam\Downloads\incoming\hello.txt"),
                numberofpagesSpecified = true,
                numberofpages = 1,
                doctype = "Holter",
                docdesc = "Hello from api",
                contenttype = "text/plain",
                status = 'A',
                appointmentNo = 0,
                doccreator = "999998",
                programId = 10034,
                responsible = ""

            });
        }

        private static void GetBillingCodeDetails()
        {
            BillingService.BillingWsClient wsClient = new BillingService.BillingWsClient();
            wsClient.Endpoint.EndpointBehaviors.Add(new MyBehavior(security));
            wsClient.Open();
            try
            {
                var result = wsClient.getServiceCode("A001A");
                foreach (var serviceCode in result)
                {
                    Console.WriteLine(serviceCode.serviceCode);
                }
            } catch (Exception e)
            {
                Console.WriteLine("Caught Exception " + e.Message);
            }
        }

        private static void GetMultiBillingCodeDetails()
        {
            BillingService.BillingWsClient wsClient = new BillingService.BillingWsClient();
            wsClient.Endpoint.EndpointBehaviors.Add(new MyBehavior(security));
            wsClient.Open();
            try
            {
                var result = wsClient.getServiceCodes(new string[] { "A001A","A002A" });
                foreach (var serviceCode in result)
                {
                    Console.WriteLine(serviceCode.serviceCode);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Caught Exception " + e.Message);
            }
        }

        private static void CreateInvoice()
        {
            BillingService.BillingWsClient wsClient = new BillingService.BillingWsClient();
            wsClient.Endpoint.EndpointBehaviors.Add(new MyBehavior(security));
            wsClient.Open();
            try
            {
                var result = wsClient.createBillingInvoice(new BillingService.billingInvoice
                {
                    creatorProviderNo = security.provider.providerNo,
                    demographicNo = new string[] { "1" },
                    facilityNum = "3866",
                    diagnosisCodes = new string[] { "457" },
                    serviceCodes = new string[] { "A001A" },
                    providerNo = "2",
                    serviceDate = new DateTime(2018, 09, 22),
                    serviceDateSpecified = true
                });
                Console.WriteLine("Total Amount is: {0}", result);
            } catch (Exception e)
            {
                Console.WriteLine("Error::: {0}", e.Message);
            }
        }

        private static void AddAppointment()
        {
            ScheduleService.ScheduleWsClient ss = new ScheduleService.ScheduleWsClient();
            ss.Endpoint.EndpointBehaviors.Add(new MyBehavior(security));
            ss.Open();
            try
            {
                var result = ss.addAppointment(new ScheduleService.appointmentTransfer() {
                    name = "READING SCHEDULE",
                    appointmentStartDateTimeSpecified = true,
                    appointmentEndDateTimeSpecified = true,
                    appointmentStartDateTime = new DateTime(2018, 09, 28, 12, 0, 0),  //yyyy,mm,dd,hh,mm,ss
                    appointmentEndDateTime = new DateTime(2018, 09, 28, 12, 30, 0),
                    status = "t",
                    notes = "automated",
                    providerNo = "999998"
                });

                Console.WriteLine("The result is: "+result);

            } catch (Exception e) {
                Console.WriteLine("Caught Exception: " + e.Message);
            }
        }

        private static void GetAppointments()
        {
            ScheduleService.ScheduleWsClient ss = new ScheduleService.ScheduleWsClient();
            ss.Endpoint.EndpointBehaviors.Add(new MyBehavior(security));
            ss.Open();
            try
            {
                DateTime startDate = new DateTime(2018, 08, 22);
                DateTime endDate = new DateTime(2018, 10, 01);
                var result = ss.getAppointmentsByDateRangeAndCreator(startDate, endDate, "corcare_service", true); 
                foreach (var appointment in result)
                {
                    Console.WriteLine("Appointment name: {0} created by {1}", appointment.name, appointment.creator);
                }
            } catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }



    }
}
