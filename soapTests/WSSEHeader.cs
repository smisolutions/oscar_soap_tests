﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace soapTests
{
    class WSSEHeader : System.ServiceModel.Channels.MessageHeader
    {

        const string ns = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
        const string name = "Security";
        private string securityId;
        private string securityToken;
    


        public override string Name
        {
            get { return name; }
        }

        public override string Namespace
        {
            get { return ns; }
        }

        public WSSEHeader(string securityId, string securityToken)
        {
            this.securityId = securityId;
            this.securityToken = securityToken;
        }

        protected override void OnWriteHeaderContents(XmlDictionaryWriter writer, MessageVersion messageVersion)
        {
            writer.WriteStartElement("UsernameToken");

            writer.WriteStartElement("Username");
            writer.WriteString(securityId);
            writer.WriteEndElement();

            writer.WriteStartElement("Password");
            writer.WriteAttributeString("Type", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText");
            writer.WriteString(securityToken);
            writer.WriteEndElement();

            writer.WriteEndElement();

        }
    }
}
